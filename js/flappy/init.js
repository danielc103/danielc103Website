﻿
var game = new Phaser.Game(800, 490, Phaser.AUTO, 'game_div');


var score = 0;

game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('game', gameState);

// Start with the 'load' state
game.state.start('load');