﻿var loadState = {
    preload: function () {
        game.stage.backgroundColor = "#2C2A29";
        game.load.image('bird', '/pictures/fsu.png');
        game.load.image('pipe', '/pictures/pipe.png');
        game.load.audio('jump', '/media/quick_fart_x.wav');
    },

    create: function () {
        
        this.game.state.start('menu');
    }
};