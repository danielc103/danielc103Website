﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(daniel103Website.Startup))]
namespace daniel103Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
